---
title: "gulp, webpack 비교"
categories:
  - 자동화 툴
tags:
  - node
  - gulp
  - webpack
---


## 자동화 빌드 도구

퍼블에서 자동화 빌드를 쓰는 이유는 head, header, footer, sass 변환 등 페이지가 많아짐에 늘어나는 반복작업들을 설정파일을 통해 편리하게 관리할 수 있기 때문에 사용한다.

* webpack : 모듈 번들러 기반 (필요한 의존성에 대해 정확하게 추적하여 해당하는 의존성을 그룹핑 해주는 도구)


## 2. _config.yml, .gitlab-ci.yml 파일 설정하기

git push 모든 파일을 업로드 하면 `pipeline failed` 결과가 나타난다.

.gitlab-ci.yml 파일을 수정해야한다.

초기 `.gitlab-ci.yml` 파일
  ```ruby
  image: ruby:2.5.0

  pages:
    script:
    - gem install jekyll
    - jekyll build -d public
    artifacts:
      paths:
      - public
    only:
    - master
  ```

  > `gem` 명령어는 node.js npm 같은 필요한 기능이 있을 때 추가하여 관리할 수 있도록 해주는 프로그램이다.


  수정된 `.gitlab-ci.yml` 파일
  ```ruby
  image: ruby:latest

variables:
  JEKYLL_ENV: production

pages:
  script:
  - bundle install
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master

  ```

  달라진건 `bundle` 이다.

  >`bundler`는 gem과 그 gem의 버전을 설치하고 추적하는 것으로 일관성 있는 ruby 환경을 관리해준다.

  code 받은 것 중 `gemfile` 이 포함되어 있는데 열어보면 `gemspec` 로 gem 들의 정보를 가지고 있기때문에 bundle 로 할 수 있게 바꾸어 주어야한다.

  `gemfile`
  ```ruby
  source "https://rubygems.org"
  gemspec
  ```

  마지막으로 _config.yml 에서 url 등 환경에 맞게 수정하면 된다.

  ```ruby
# theme                  : "minimal-mistakes-jekyll"
# remote_theme           : "mmistakes/minimal-mistakes"
minimal_mistakes_skin    : "dark" # "air", "aqua", "contrast", "dark", "dirt", "neon", "mint", "plum", "sunrise"

# Site Settings
locale                   : "utf-8"
title                    : "?Blog"
title_separator          : "-"
subtitle                 : "" # site tagline that appears below site title in masthead
name                     : "Seo young"
description              : "An amazing website."
url                      : "https://youngk.gitlab.io/" # the base hostname & protocol for your site e.g. "https://mmistakes.github.io"
baseurl                  : "/youngkblog" # the subpath of your site, e.g. "/blog"
```

위와 같이 수정하여 테마를 적용했다.